<?php
/*
Plugin Name: Direct Customers According To Postcode.
Plugin URI: 
Description: Direct Customers According To Postcode. 
Author: Agile Solutions PK
Version: 1.8
Author URI: http://agilesolutionspk.com
*/

if(!(class_exists('Aspk_Direct_Customers_According_To_Postcode'))){
	class Aspk_Direct_Customers_According_To_Postcode{
		
		function __construct(){
			register_activation_hook( __FILE__, array(&$this, 'install') );
			add_action('admin_menu', array(&$this,'admin_menu'));
			add_action('wp_enqueue_scripts', array(&$this, 'wp_enqueue_scripts') );
			add_action('admin_enqueue_scripts', array(&$this, 'wp_enqueue_scripts') );
			add_shortcode( 'aspk_postcode_products', array(&$this,'process_postcode_post' ));
			add_action( 'init', array(&$this,'process_post' ));
			add_action( 'wp_ajax_aspk_save_session',  array(&$this,'save_postal_code' ));
			add_action( 'wp_ajax_nopriv_aspk_save_session', array(&$this,'save_postal_code' ));
			add_filter( 'the_content', array(&$this,'my_the_content_filter' ));
			add_action( 'pre_get_posts', array(&$this,'my_tax_query' ));
			add_action( 'pre_get_posts', array(&$this,'my_shop_query' ));
			add_filter( 'woocommerce_checkout_fields' , array(&$this,'custom_override_checkout_fields' ));
		}
		
		function custom_override_checkout_fields( $fields ) {
			/* $postcode = $_SESSION['agile_postcode'];
			$pcode = str_replace('PC-','',$postcode); */
			
			$pcode = $_SESSION['agile_postcode_full'];
			
			$fields['billing']['billing_postcode'] = array(   //replace billing_postcode with your custom field name or something ;)
				'label' => __('Postcode', 'woocommerce'),              //replace Postcode with the field label
				'placeholder' => _x('Postcode', 'placeholder', 'woocommerce'),         //replace Postcode with your custom default value
				'required' => false,
				'clear' => false,
				'type' => 'select',
				'class' => array('form-row-wide'),
				'options' => array(
						   'option_a' => __($pcode, 'woocommerce' ),
				)
			);
			return $fields;
		}
		
		function my_tax_query( &$query ) {
			
			
				if(isset($query->tax_query) && isset($query->query['post_type']) && ! $query->is_main_query() && !is_admin() ){
					
					if($query->query['post_type'] == 'product' && isset($query->tax_query->queries[0]['terms'][0])){
						$post_code = '';
						if(isset($_SESSION['agile_postcode'])){
							$postcode = $_SESSION['agile_postcode'];
							$p_code = strtolower($postcode);
							$post_code = str_replace(' ','-',$p_code);
						}
						$catg = array();
						$x = $query->tax_query->queries[0]['terms'][0];
						$catg[] = $x;
						$catg[] = $post_code;
						$catg[] = 'pc-all';
						
						$query->set( 'tax_query', array(array(
							'taxonomy' => 'product_cat',
							'field' => 'slug',
							'terms' => $catg , 
							'operator' => 'IN'
						)));
					}
				}
			
		}
		
		function my_shop_query( &$query ) {
			
			
				if(isset($query->query['post_type'])  && !is_admin() && $query->is_main_query() && is_post_type_archive()){
					
					if($query->query['post_type'] == 'product'){
						$post_code = '';
						if(isset($_SESSION['agile_postcode'])){
							$postcode = $_SESSION['agile_postcode'];
							$p_code = strtolower($postcode);
							$post_code = str_replace(' ','-',$p_code);
						}
						$catg = array();
						$catg[] = $post_code;
						$catg[] = 'pc-all';
						
						$query->set( 'tax_query', array(array(
							'taxonomy' => 'product_cat',
							'field' => 'slug',
							'terms' => $catg , 
							'operator' => 'IN'
						)));
					}
				}
			
		}
		
		
		
		function process_postcode_post() {
			global $post;
			
			$postcode = $_SESSION['agile_postcode'];
			$p_code = strtolower($postcode);
			$post_code = str_replace(' ','-',$p_code);
			echo do_shortcode('[product_category category="'.$post_code.'"]');
			
		}
		
		function get_all_product_catg(){
			
			$product_categories = get_terms( 'product_cat' );
			$count = count($product_categories);
			
			if ( $count > 0 ){
				return $product_categories;
			}
		}
		  
		function save_postal_code(){
			$p_code_4 = $_POST['post_code'];
			$p_code = substr($p_code_4, 0, 4); 
			/*
			$post_code = strtolower($p_code);
			$post_code = str_replace(' ','-',$post_code);
			
			$all_catg = $this->get_all_product_catg();
			if($all_catg){
				foreach($all_catg as $cat){
					$catname = $cat->slug;
					$postal_code = 'pc-'.$post_code;
					if($catname == $postal_code){
						$_SESSION['agile_postcode'] = 'PC-'.$p_code;
						$ret = array('act' => 'show_postal_code','st'=>'ok','msg'=>'Congratulations! We deliver in your area');
						echo json_encode($ret);
						exit;
					}
				}
			}
			$ret = array('act' => 'show_postal_code','st'=>'fail','msg'=>'Unfortunately we do not currently deliver to your area');
			echo json_encode($ret);
			exit; */
			$_SESSION['agile_postcode_full'] = $p_code_4;
			$_SESSION['agile_postcode'] = 'PC-'.$p_code;
			$ret = array('act' => 'show_postal_code','st'=>'ok','msg'=>'Congratulations! We deliver in your area');
			echo json_encode($ret);
			exit;
		}
		
		function my_the_content_filter($content) {
			global $post;
			
			if(isset($_SESSION['agile_postcode'])){
				$post_code = $_SESSION['agile_postcode'];
			}else{
				return $content;
			}
			$page_id = $post->ID;
			$page_title = $post->post_title;
			$post_type = $post->post_type;
			
			if( $post_type == 'product' ){
				return $content;
			}
			
			if($post_type == 'page'){
				$assignment = get_option( 'aspk_page_postcode', array() );
				if (array_key_exists($post->post_parent, $assignment)) return $content;
				if (array_key_exists($page_id, $assignment)) return $content;
			}
			
			if( $page_title == 'Home' || $page_title == 'Shop' || $page_title == 'Contact' || $page_title == 'Basket' ||$page_title == 'Checkout' ||$page_title == 'About' || $page_title == 'Privacy' || $page_title == 'Terms & Conditions'|| $page_title == 'Blog'|| $page_title == 'FAQ'|| $page_title == 'Website Terms of Use'|| $page_title == 'Terms Sales' || $page_title == 'Boxes' || $page_title == 'Meet the Locals'){
				return $content;
			}
			
			$term = get_the_terms($page_id,'category'); 
			if(!empty($term)){
				foreach($term as $t){
					$category = $t->name;
					if($post_code == 'PC-ALL' || $post_code == 'pc-all') return $content;
					if($post_code == $category){
						return $content;				
					}else{
						$content = '<div style="clear:left;min-height:30em;padding:1em;background-color:white;"><h2 style="text-align:center;">This is not available in your area.</h2></div>';
						return $content;
					}
				}
			}else{
				$p_code = get_post_meta($page_id, 'postcode_cat',true);
				if($post_code == $p_code){
					return $content;				
				}else{
					$content = '<div style="clear:left;min-height:30em;padding:1em;background-color:white;"><h2 style="text-align:center;">This is not available in your area.</h2></div>';
					return $content;
				}
			} 
			return $content;
		
		}
			
		function process_post(){
			if(! session_id()){
				 session_start();
			}
			$uid = get_current_user_id();
			if($uid){
				$x = get_user_meta($uid,'billing_postcode',true);
				$_SESSION['agile_postcode'] = 'PC-'.$x;
			}
		}
		
		function install(){
			
		}
		
		function admin_menu(){
			add_menu_page('Select Pages', 'Select Pages', 'manage_options', 'aspk_add_pages', array(&$this, 'add_pages') );
		}
		
		function add_pages(){
			
			$assignment = get_option( 'aspk_page_postcode', array() );
			
			if(isset($_POST['aspk_sbtn'])){
				$postcode = $_POST['aspk_select_post'];
				$page = $_POST['aspk_select_page'];
				
				if($postcode == 'unassign' && !empty($assignment)){
					
					$new_assignment = array();
					
					foreach($assignment as $key => $value){ //$value is array of postcodes
						
						if(in_array($key, $page)) continue;
						
						$new_assignment[$key] = $value;
						
					} 
					
					update_option( 'aspk_page_postcode', $new_assignment );
					$assignment = get_option( 'aspk_page_postcode', array() );
					
				} elseif($page && $postcode){
					if($postcode != 'unassign'){
						foreach($page as $pageid){
							
							if(isset($assignment[$pageid])){
								$tmpval = $assignment[$pageid];
							}else{
								$tmpval = array();
							}
							
							$tmpval[] = $postcode;
							$assignment[$pageid] = $tmpval;
							
							update_option( 'aspk_page_postcode', $assignment );
							$assignment = get_option( 'aspk_page_postcode', array() );
							
						}
					}
				}
			}
			?>
			<div class="tw-bs container">
				<div class="row">
					<div class="col-md-12">
						<h2 style="text-align:center;">Select Pages</h2>
					</div>
				</div>
				<form method="post" action="">
					<div class="row">
						<div class="col-md-1">
						<b>Page</b>
						</div>
						<div class="col-md-11">
						<?php 
							//$product_categories = get_all_category_ids();
							$args1 = array(
								'type'                     => 'post',
								'child_of'                 => 0,
								'parent'                   => '',
								'orderby'                  => 'name',
								'order'                    => 'ASC',
								'hide_empty'               => 0,
								'hierarchical'             => 1,
								'exclude'                  => '',
								'include'                  => '',
								'number'                   => '',
								'taxonomy'                 => 'category',
								'pad_counts'               => false 

							); 
							$product_categories = get_categories( $args1 );
							
							$args = array(
									'sort_order' => 'asc',
									'sort_column' => 'post_title',
									'hierarchical' => 1,
									'exclude' => '',
									'include' => '',
									'meta_key' => '',
									'meta_value' => '',
									'authors' => '',
									'child_of' => 0,
									'parent' => -1,
									'exclude_tree' => '',
									'number' => '',
									'offset' => 0,
									'post_type' => 'page',
									'post_status' => 'publish'
								); 
							$pages = get_pages($args); 
							?>
							<select name="aspk_select_page[]" multiple required>
								
								<?php
								foreach ( $pages as $page ) {
								?><option value="<?php echo $page->ID; ?>"><?php echo $page->post_title; ?></option><?php
									
								}
								?>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-1" style="margin-top:1em;">
						<b>Postcode</b>
						</div>
						<div class="col-md-11" style="margin-top:1em;">
							<select name="aspk_select_post" required>
								<option value="">Select</option>
								<?php
								foreach ( $product_categories as $cat ) {
									$prod = substr($cat->name, 0, 3);
									if($prod == 'PC-'){
										?><option value="<?php  echo $cat->name;?>"><?php echo $cat->name; ?></option><?php
									}
								}
								?>
								<option value="unassign">Un-Assign All</option>
							</select>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<input type="submit" value="Save" name="aspk_sbtn" class="btn btn-primary">
						</div>
					</div>
				</form>
				<?php 
							$assignment = get_option( 'aspk_page_postcode', array() );

				?>
								<div class="row">
									<div class="col-md-12" style="margin-top:1em;">
										<h2 style="text-align:center;">Current Assignments</h2>
									</div>
								</div>
								<div class="row">
									<div class="col-md-2" style="margin-top:1em;margin-bottom:1em;"><b>Postcode</b></div>
									<div class="col-md-10" style="margin-top:1em;margin-bottom:1em;"><b>Page Title</b></div>
								</div>
							<?php
								if($assignment){
									foreach($assignment as $page_id => $value){ //$value is array of postcodes
										
										if(empty($value)) continue;
										
										$page = get_post( $page_id );
										
										foreach($value as $post_code){
											
							?>
											<div class="row">
												<div class="col-md-2"><?php echo $post_code; ?></div>
												<div class="col-md-10"><?php echo $page->post_title; ?></div>
											</div>
										&nbsp;
									
							<?php 		}

									}
								}									
								
							?>
				</div>
			<?php
		}
		
		function wp_enqueue_scripts(){
			wp_enqueue_script('jquery');
			wp_enqueue_script('js-fe-bootstrap',plugins_url('js/js-agile-bootstrap.js', __FILE__)
			);
			wp_enqueue_style( 'agile-fe-bootstrap', plugins_url('css/agile-bootstrap.css', __FILE__) );
		}
		
	}// class ends
	
}//if ends
 new Aspk_Direct_Customers_According_To_Postcode();