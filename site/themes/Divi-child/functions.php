<?php

// =============================================================================
// FUNCTIONS.PHP
// -----------------------------------------------------------------------------
// Overwrite or add your own custom functions to Divi in this file.
// =============================================================================

function use_parent_theme_stylesheet() {
    // Use the parent theme's stylesheet
    return get_template_directory_uri() . '/style.css';
}

function my_theme_styles() {
    $themeVersion = wp_get_theme()->get('Version');

    // Enqueue our style.css with our own version
    wp_enqueue_style('child-theme-style', get_stylesheet_directory_uri() . '/style.css',
        array(), $themeVersion);
}

// Filter get_stylesheet_uri() to return the parent theme's stylesheet 
add_filter('stylesheet_uri', 'use_parent_theme_stylesheet');

// Enqueue this theme's scripts and styles (after parent theme)
add_action('wp_enqueue_scripts', 'my_theme_styles', 20);

/**
 * Adds a link to the Secondary Menu based on the 
 * user's logged in status.  This is to be placed
 * in your theme's functions.php file or in your 
 * own plugin file.
 * 
 * This example works for the WordPress Divi theme.
 */
function divi_wp_nav_menu_items($items, $args) {

	// Make sure this is the Secondary Menu.
	// You may need to modify this condition
	// depending on your theme.
	if ($args->theme_location == 'secondary-menu') {
	
		// CSS class to use for <li> item.
		$class = 'menu-item';

		if (is_user_logged_in()) {
			// User is logged in, link to welcome page.
			$extra = '
			<li id="menu-item-logged-in-user" class="'.$class.'">
				<a href="http://www.amorelocal.com/profile/">
					'.__('Hello').' '.wp_get_current_user()->user_login.'
				</a>
			</li>

			<li id="menu-item-logged-in-user" class="'.$class.'">
				<a href="http://www.amorelocal.com/logout/" >
					'.__('Logout').'
				</a>
			</li>
			';
		} else {
			// User is guest, link to login / Register page.
			$extra = '
			<li id="menu-item-logged-out-user" class="'.$class.'">
				   
                                 <a href="http://www.amorelocal.com/login/" class="popup-login">
					'.__('Login').'
</a>
			</li>
			<li id="menu-item-logged-out-user" class="'.$class.'">
				   
                                       <a href="http://www.amorelocal.com/profile/register/" class="popup-register">
					'.__('Register').'
				</a>
			</li>
			';
		}

		// Add extra link to existing menu.
		$items = $items . $extra; 
	}
	// Return menu items.
	return $items;		

	
}

// Hook into wp_nav_menu_items.
add_filter('wp_nav_menu_items', 'divi_wp_nav_menu_items', 10, 2 );

// rename the "Have a Coupon?" message on the checkout page
function woocommerce_rename_coupon_message_on_checkout() {

	return 'Have a Promotional Code?' . ' <a href="#" class="showcoupon">' . __( 'Click here to enter your code', 'woocommerce' ) . '</a>';
}
add_filter( 'woocommerce_checkout_coupon_message', 'woocommerce_rename_coupon_message_on_checkout' );

// rename the coupon field on the checkout page
function woocommerce_rename_coupon_field_on_checkout( $translated_text, $text, $text_domain ) {

	// bail if not modifying frontend woocommerce text
	if ( is_admin() || 'woocommerce' !== $text_domain ) {
		return $translated_text;
	}

	if ( 'Coupon code' === $text ) {
		$translated_text = 'Enter Code';
	
	} elseif ( 'Apply Coupon' === $text ) {
		$translated_text = 'Apply Promotion';
	}

	return $translated_text;
}
add_filter( 'gettext', 'woocommerce_rename_coupon_field_on_checkout', 10, 3 );

add_action( 'woocommerce_before_cart_table', 'woo_add_continue_shopping_button_to_cart' );

function woo_add_continue_shopping_button_to_cart() {
    $shop_page_url = get_permalink( woocommerce_get_page_id( 'shop' ) );
    echo '<div class="return-to-shop">';
    echo ' <a href="' . $shop_page_url . '" class="button wc-backward">Continue Shopping</a>';
    echo '</div>';
}


// Change 'add to cart' text on single product page (depending on product IDs)
add_filter( 'woocommerce_product_single_add_to_cart_text', 'che_id_multiple_add_to_cart_text' );
function che_id_multiple_add_to_cart_text( $default ) {
    if ( get_the_ID() == 7946 ) {
        return __( 'Add box', 'your-slug' );
    } elseif ( get_the_ID() == 7933 ) {
    	return __( 'Add box', 'your-slug' );
    } elseif ( get_the_ID() == 7905 ) {
    	return __( 'Add box', 'your-slug' );
    } elseif ( get_the_ID() == 7900 ) {
    	return __( 'Add box', 'your-slug' );
    } elseif ( get_the_ID() == 8548 ) {
    	return __( 'Add box', 'your-slug' );
    } elseif ( get_the_ID() == 8556 ) {
    	return __( 'Add box', 'your-slug' );
    } else {
        return $default;
    }
}


?>