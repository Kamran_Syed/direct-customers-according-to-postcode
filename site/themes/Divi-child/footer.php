<script>
jQuery(function($){
    $('.et_pb_more_button').attr('target', '_blank');
});
</script>

<?php if ( 'on' == et_get_option( 'divi_back_to_top', 'false' ) ) : ?>

	<span class="et_pb_scroll_top et-pb-icon"></span>

<?php endif;

if ( ! is_page_template( 'page-template-blank.php' ) ) : ?>

			<footer id="main-footer">
				<?php get_sidebar( 'footer' ); ?>


		<?php
			if ( has_nav_menu( 'footer-menu' ) ) : ?>

				<div id="et-footer-nav">
					<div class="container">
<a href="http://www.paymentsense.co.uk/card-payment-security/" target="_blank"rel="nofollow">
  <img src="http://www.paymentsense.co.uk/CardLogos/645x50/02.white/PaymentSense_Standard_645x50.png" alt="Payments secured by Paymentsense Merchant Services" align="right" style="padding-top:10px;">
</a>
						<?php
							wp_nav_menu( array(
								'theme_location' => 'footer-menu',
								'depth'          => '1',
								'menu_class'     => 'bottom-nav',
								'container'      => '',
								'fallback_cb'    => '',
							) );
						
						?>



					</div>
				</div> <!-- #et-footer-nav -->
					
			<?php endif; ?>

				<div id="footer-bottom">
					<div class="container clearfix" />

				<?php
					if ( false !== et_get_option( 'show_footer_social_icons', true ) ) {
						get_template_part( 'includes/social_icons', 'footer' );
					}
				?>

						<p id="footer-info"><?php printf( __( 'Designed by %1$s | Powered by %2$s', 'Divi' ), '<a href="http://www.elegantthemes.com" title="Premium WordPress Themes">Elegant Themes</a>', '<a href="http://www.wordpress.org">WordPress</a>' ); ?></p>
					</div>	<!-- .container -->
				</div>
			</footer> <!-- #main-footer -->
		</div> <!-- #et-main-area -->
	

<?php endif; // ! is_page_template( 'page-template-blank.php' ) ?>

	</div> <!-- #page-container -->

	<?php wp_footer();
	if(isset($_SESSION['agile_postcode_full'])){
		$post_code = $_SESSION['agile_postcode_full'];
		?>
			<script>
				jQuery( document ).ready(function() {
					jQuery("#postcode").val('<?php echo $post_code; ?>');
				});
			</script>
		<?php
	}
if(is_page('Home')){	
	?>
<script>
	
 	var e = document.getElementsByClassName("et_pb_promo_button")[0];
	var d = document.createElement('label');
	d.className='et_pb_promo_button et_pb_button';
	d.innerHTML = "Get Started";
	e.parentNode.insertBefore(d, e);
	e.parentNode.removeChild(e);
	document.getElementsByClassName("et_pb_promo_button et_pb_button")[0].htmlFor="modal-1";

</script>
<?php } ?>

<input class="modal-state" id="modal-1" type="checkbox" />
    <div class="modal">
    <label id="modal-1" class="modal_bg"></label>
    <div class="modal__inner">
    <label class="modal__close" for="modal-1"></label>
    <div class="centerImage"><img src="http://www.amorelocal.com/wp-content/uploads/2014/12/logo3.png" alt="AmoreLocal" id="logo"/></div>
    <div class="content">
        <input type="text" name="postcode" id="postcode" class="postcode" placeholder="Please enter your postcode" required >
	<div><p id="msg" ></p></div>
        <div> <button id="btn" class="btn" type="submit" onclick="isValidPostcode()">Submit</button></div>
    	
    </div>
  </div>
  </div>
<script>
 
function isValidPostcode() {
    var p = document.getElementById("postcode").value; 
	jQuery('#btn').attr('disabled','disabled'); 	
    var postcodeRegEx = /^[A-Z]{1,2}[0-9]{1,2}[A-Z]{0,1} ?[0-9][A-Z]{2}$/i; 
    if(! postcodeRegEx.test(p)){
		jQuery('#msg').html('Post code is invalid.');
		jQuery('#btn').attr('disabled',false);
		return false;
	}else{
		document.getElementById("msg").innerHTML="<img style='width:3em;'  src='<?php echo get_bloginfo('template_url') ?>/images/loader.gif' />"; 
		// save in session
		var data = {
				'action': 'aspk_save_session',
				'post_code': p      
			};
	
		var ajaxurl = '<?php echo admin_url( 'admin-ajax.php' ); ?>'
		jQuery.post(ajaxurl, data, function(response) {
			jQuery('#btn').attr('disabled',false);
			obj = JSON.parse(response);
			 if(obj.st == 'ok'){	
				jQuery("#msg").html(obj.msg);
				btn =document.getElementById("btn");
				btn.innerHTML="Shop Now";
					btn.style.background="#FF3300";
				btn.onclick=function(){window.location='<?php echo home_url();?>/boxes/'};
			 }
			 if(obj.st == 'fail'){
				jQuery("#msg").html(obj.msg);
				btn =document.getElementById("btn");
				btn.innerHTML="Contact Us";
				btn.style.background="#FF3300";
				btn.onclick=function(){window.location='<?php echo home_url();?>/contact'};
			 }

		});
	}

   /*  else if(postcodeRegEx.test(p)){
	document.getElementById("msg").innerHTML="Contact us to find if we can deliver in your area"; 
	btn =document.getElementById("btn");
	btn.innerHTML="Contact Us";
	btn.style.background="#FF3300";
	btn.onclick=function(){window.location='http://www.amorelocal.com/contact'};
	}
    else
	document.getElementById("msg").innerHTML="Please enter a valid postcode"; */
	 
}

</script>



</body>
</html>
