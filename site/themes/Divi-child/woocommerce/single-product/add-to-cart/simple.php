<?php
/**
 * Simple product add to cart
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.1.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

global $product;

if ( ! $product->is_purchasable() ) {
	return;
}

?>

<?php
	// Availability
	$availability      = $product->get_availability();
	$availability_html = empty( $availability['availability'] ) ? '' : '<p class="stock ' . esc_attr( $availability['class'] ) . '">' . esc_html( $availability['availability'] ) . '</p>';

	echo apply_filters( 'woocommerce_stock_html', $availability_html, $availability['availability'], $product );
?>

<?php if ( $product->is_in_stock() ) : ?>

	<?php do_action( 'woocommerce_before_add_to_cart_form' ); ?>

	<form class="cart" method="post" enctype='multipart/form-data'>
	 	<?php do_action( 'woocommerce_before_add_to_cart_button' ); 
		
		?>

	 	<?php
	 		if ( ! $product->is_sold_individually() )
	 			woocommerce_quantity_input( array(
	 				'min_value' => apply_filters( 'woocommerce_quantity_input_min', 1, $product ),
	 				'max_value' => apply_filters( 'woocommerce_quantity_input_max', $product->backorders_allowed() ? '' : $product->get_stock_quantity(), $product )
	 			) );
				$postcode = $_SESSION['agile_postcode'];
				global $post;
				$terms = get_the_terms( $post->ID, 'product_cat' );
				foreach ($terms as $term) {
					$product_cat_name = $term->name;
					if($product_cat_name == $postcode){
						?>
						<script>
							jQuery( document ).ready(function() {
								jQuery("#aspk_postid").show();
								jQuery("#aspk_postid_msg").hide();
							});
						</script>
						
					<?php	
					}
				}
			
		?>
		<script>
			function show_error_msg(){
				alert('This is not available in your area.');
			}
			
			jQuery( document ).ready(function() {
				jQuery(".product_meta .posted_in").hide();
			});
		</script>
		<button type="submit" id="aspk_postid" style="display:none;" class="single_add_to_cart_button button alt" ><?php echo $product->single_add_to_cart_text(); ?></button>
		<button type="button" id="aspk_postid_msg" class="single_add_to_cart_button button alt" onclick="show_error_msg()"><?php echo $product->single_add_to_cart_text(); ?></button>
	 	<input type="hidden" name="add-to-cart" value="<?php echo esc_attr( $product->id ); ?>" />
		
	 	

		<?php do_action( 'woocommerce_after_add_to_cart_button' ); ?>
	</form>

	<?php do_action( 'woocommerce_after_add_to_cart_form' ); ?>

<?php endif; ?>
